// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_LEWIS2_Paintball_Lewis2HUD_generated_h
#error "Paintball_Lewis2HUD.generated.h already included, missing '#pragma once' in Paintball_Lewis2HUD.h"
#endif
#define PAINTBALL_LEWIS2_Paintball_Lewis2HUD_generated_h

#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_SPARSE_DATA
#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_RPC_WRAPPERS
#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintball_Lewis2HUD(); \
	friend struct Z_Construct_UClass_APaintball_Lewis2HUD_Statics; \
public: \
	DECLARE_CLASS(APaintball_Lewis2HUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paintball_Lewis2"), NO_API) \
	DECLARE_SERIALIZER(APaintball_Lewis2HUD)


#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPaintball_Lewis2HUD(); \
	friend struct Z_Construct_UClass_APaintball_Lewis2HUD_Statics; \
public: \
	DECLARE_CLASS(APaintball_Lewis2HUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paintball_Lewis2"), NO_API) \
	DECLARE_SERIALIZER(APaintball_Lewis2HUD)


#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintball_Lewis2HUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintball_Lewis2HUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_Lewis2HUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_Lewis2HUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_Lewis2HUD(APaintball_Lewis2HUD&&); \
	NO_API APaintball_Lewis2HUD(const APaintball_Lewis2HUD&); \
public:


#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_Lewis2HUD(APaintball_Lewis2HUD&&); \
	NO_API APaintball_Lewis2HUD(const APaintball_Lewis2HUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_Lewis2HUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_Lewis2HUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintball_Lewis2HUD)


#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_PRIVATE_PROPERTY_OFFSET
#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_9_PROLOG
#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_SPARSE_DATA \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_RPC_WRAPPERS \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_INCLASS \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_SPARSE_DATA \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_INCLASS_NO_PURE_DECLS \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTBALL_LEWIS2_API UClass* StaticClass<class APaintball_Lewis2HUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2HUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
