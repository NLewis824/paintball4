// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector2D;
#ifdef PAINTBALL_LEWIS2_FOWActor_generated_h
#error "FOWActor.generated.h already included, missing '#pragma once' in FOWActor.h"
#endif
#define PAINTBALL_LEWIS2_FOWActor_generated_h

#define Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_SPARSE_DATA
#define Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execrevealSmoothCircle) \
	{ \
		P_GET_STRUCT_REF(FVector2D,Z_Param_Out_pos); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_radius); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->revealSmoothCircle(Z_Param_Out_pos,Z_Param_radius); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetSize) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->getSize(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetSize) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_s); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setSize(Z_Param_s); \
		P_NATIVE_END; \
	}


#define Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execrevealSmoothCircle) \
	{ \
		P_GET_STRUCT_REF(FVector2D,Z_Param_Out_pos); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_radius); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->revealSmoothCircle(Z_Param_Out_pos,Z_Param_radius); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetSize) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->getSize(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetSize) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_s); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setSize(Z_Param_s); \
		P_NATIVE_END; \
	}


#define Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFOWActor(); \
	friend struct Z_Construct_UClass_AFOWActor_Statics; \
public: \
	DECLARE_CLASS(AFOWActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paintball_Lewis2"), NO_API) \
	DECLARE_SERIALIZER(AFOWActor)


#define Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAFOWActor(); \
	friend struct Z_Construct_UClass_AFOWActor_Statics; \
public: \
	DECLARE_CLASS(AFOWActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paintball_Lewis2"), NO_API) \
	DECLARE_SERIALIZER(AFOWActor)


#define Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFOWActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFOWActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFOWActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFOWActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFOWActor(AFOWActor&&); \
	NO_API AFOWActor(const AFOWActor&); \
public:


#define Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFOWActor(AFOWActor&&); \
	NO_API AFOWActor(const AFOWActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFOWActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFOWActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFOWActor)


#define Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__m_squarePlane() { return STRUCT_OFFSET(AFOWActor, m_squarePlane); } \
	FORCEINLINE static uint32 __PPO__m_dynamicTexture() { return STRUCT_OFFSET(AFOWActor, m_dynamicTexture); } \
	FORCEINLINE static uint32 __PPO__m_dynamicMaterial() { return STRUCT_OFFSET(AFOWActor, m_dynamicMaterial); } \
	FORCEINLINE static uint32 __PPO__m_dynamicMaterialInstance() { return STRUCT_OFFSET(AFOWActor, m_dynamicMaterialInstance); }


#define Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_11_PROLOG
#define Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_PRIVATE_PROPERTY_OFFSET \
	Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_SPARSE_DATA \
	Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_RPC_WRAPPERS \
	Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_INCLASS \
	Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_PRIVATE_PROPERTY_OFFSET \
	Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_SPARSE_DATA \
	Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_INCLASS_NO_PURE_DECLS \
	Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTBALL_LEWIS2_API UClass* StaticClass<class AFOWActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Lewis2_Source_Paintball_Lewis2_FOWActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
