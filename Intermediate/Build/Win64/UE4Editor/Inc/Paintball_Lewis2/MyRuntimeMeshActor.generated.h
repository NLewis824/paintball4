// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_LEWIS2_MyRuntimeMeshActor_generated_h
#error "MyRuntimeMeshActor.generated.h already included, missing '#pragma once' in MyRuntimeMeshActor.h"
#endif
#define PAINTBALL_LEWIS2_MyRuntimeMeshActor_generated_h

#define Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_SPARSE_DATA
#define Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_RPC_WRAPPERS
#define Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyRuntimeMeshActor(); \
	friend struct Z_Construct_UClass_AMyRuntimeMeshActor_Statics; \
public: \
	DECLARE_CLASS(AMyRuntimeMeshActor, ARuntimeMeshActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paintball_Lewis2"), NO_API) \
	DECLARE_SERIALIZER(AMyRuntimeMeshActor)


#define Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMyRuntimeMeshActor(); \
	friend struct Z_Construct_UClass_AMyRuntimeMeshActor_Statics; \
public: \
	DECLARE_CLASS(AMyRuntimeMeshActor, ARuntimeMeshActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paintball_Lewis2"), NO_API) \
	DECLARE_SERIALIZER(AMyRuntimeMeshActor)


#define Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyRuntimeMeshActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyRuntimeMeshActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyRuntimeMeshActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyRuntimeMeshActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyRuntimeMeshActor(AMyRuntimeMeshActor&&); \
	NO_API AMyRuntimeMeshActor(const AMyRuntimeMeshActor&); \
public:


#define Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyRuntimeMeshActor(AMyRuntimeMeshActor&&); \
	NO_API AMyRuntimeMeshActor(const AMyRuntimeMeshActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyRuntimeMeshActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyRuntimeMeshActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyRuntimeMeshActor)


#define Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_PRIVATE_PROPERTY_OFFSET
#define Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_12_PROLOG
#define Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_PRIVATE_PROPERTY_OFFSET \
	Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_SPARSE_DATA \
	Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_RPC_WRAPPERS \
	Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_INCLASS \
	Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_PRIVATE_PROPERTY_OFFSET \
	Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_SPARSE_DATA \
	Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_INCLASS_NO_PURE_DECLS \
	Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTBALL_LEWIS2_API UClass* StaticClass<class AMyRuntimeMeshActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Lewis2_Source_Paintball_Lewis2_MyRuntimeMeshActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
