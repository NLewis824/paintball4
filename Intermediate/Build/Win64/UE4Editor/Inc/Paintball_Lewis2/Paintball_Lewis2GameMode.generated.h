// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_LEWIS2_Paintball_Lewis2GameMode_generated_h
#error "Paintball_Lewis2GameMode.generated.h already included, missing '#pragma once' in Paintball_Lewis2GameMode.h"
#endif
#define PAINTBALL_LEWIS2_Paintball_Lewis2GameMode_generated_h

#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_SPARSE_DATA
#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_RPC_WRAPPERS
#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintball_Lewis2GameMode(); \
	friend struct Z_Construct_UClass_APaintball_Lewis2GameMode_Statics; \
public: \
	DECLARE_CLASS(APaintball_Lewis2GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paintball_Lewis2"), PAINTBALL_LEWIS2_API) \
	DECLARE_SERIALIZER(APaintball_Lewis2GameMode)


#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPaintball_Lewis2GameMode(); \
	friend struct Z_Construct_UClass_APaintball_Lewis2GameMode_Statics; \
public: \
	DECLARE_CLASS(APaintball_Lewis2GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paintball_Lewis2"), PAINTBALL_LEWIS2_API) \
	DECLARE_SERIALIZER(APaintball_Lewis2GameMode)


#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	PAINTBALL_LEWIS2_API APaintball_Lewis2GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintball_Lewis2GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PAINTBALL_LEWIS2_API, APaintball_Lewis2GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_Lewis2GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PAINTBALL_LEWIS2_API APaintball_Lewis2GameMode(APaintball_Lewis2GameMode&&); \
	PAINTBALL_LEWIS2_API APaintball_Lewis2GameMode(const APaintball_Lewis2GameMode&); \
public:


#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PAINTBALL_LEWIS2_API APaintball_Lewis2GameMode(APaintball_Lewis2GameMode&&); \
	PAINTBALL_LEWIS2_API APaintball_Lewis2GameMode(const APaintball_Lewis2GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PAINTBALL_LEWIS2_API, APaintball_Lewis2GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_Lewis2GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintball_Lewis2GameMode)


#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_9_PROLOG
#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_SPARSE_DATA \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_RPC_WRAPPERS \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_INCLASS \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_SPARSE_DATA \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_INCLASS_NO_PURE_DECLS \
	Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTBALL_LEWIS2_API UClass* StaticClass<class APaintball_Lewis2GameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Lewis2_Source_Paintball_Lewis2_Paintball_Lewis2GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
