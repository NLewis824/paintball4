// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Paintball_Lewis2GameMode.h"
#include "Paintball_Lewis2HUD.h"
#include "Paintball_Lewis2Character.h"
#include "UObject/ConstructorHelpers.h"

APaintball_Lewis2GameMode::APaintball_Lewis2GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = APaintball_Lewis2HUD::StaticClass();
}
